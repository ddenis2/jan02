﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate_Around : MonoBehaviour {

    public float moveSpeed = 10;
    public float turnSpeed = 5;
    private Rigidbody rb;
    private float translation;
    private float rotation;


    private void Start()
    {
        rb = GetComponent<Rigidbody>();
       

    }

   
    private void Update()
    {

        //float rotation = Input.GetAxis("Horizontal") * rotationSpeed;

        //// Determine the number of degrees to be turned based on the input, speed and time between frames.
        //float turn = rotationSpeed * Time.deltaTime;

        //// Make this into a rotation in the y axis.
        //Quaternion turnRotation = Quaternion.Euler(0.0f, 10, 0.0f);

        //// Apply this rotation to the rigidbody's rotation.
        //rb.MoveRotation(rb.rotation * turnRotation);

        //  translation = (int)Mathf.Floor(translation * Time.deltaTime);
        //   rotation = rotation * Time.deltaTime;

        //  translation = 1.0f * speed;
        //   rotation = Input.GetAxis("Horizontal") * rotationSpeed;

        if(Input.GetKey(KeyCode.W))
            transform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
        if (Input.GetKey(KeyCode.S))
            transform.Translate(-Vector3.forward * moveSpeed * Time.deltaTime);

        if (Input.GetKey(KeyCode.A))
            transform.Rotate(Vector3.up, -turnSpeed * Time.deltaTime, Space.Self);
        if (Input.GetKey(KeyCode.D))
            transform.Rotate(Vector3.up, turnSpeed * Time.deltaTime, Space.Self);

        //Ray ray = new Ray(rb.transform.position + Vector3.up * 10.0f, Vector3.down);
        //RaycastHit hit = new RaycastHit();
        //if (Physics.Raycast(ray, out hit))
        //{
        //    rb.transform.rotation = Quaternion.FromToRotation(rb.transform.up, hit.normal) * rb.transform.rotation;
        //}

    }
    
}
